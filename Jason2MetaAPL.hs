module Jason2MetaAPL where

import Jason.JasonAST
import MetaAPL.MetaAPLAST
import Jason.JasonParser

import Data.List (intercalate)

type Path = String
type Source = String


translateAtomForm (JAtmFrm (Left v) args _) = MAtom v (map translateTerm args)
translateAtomForm (JAtmFrm (Right s) args _) = MAtom s (map translateTerm args)


translateArithTerm :: JArithTerm -> MArith
translateArithTerm (JArithTermEnd simples) = if (length simples > 1)
                                             then error "factor syntax not implemented yet"
                                             else translateSimpleArith (head simples)

translateArithTerm (JArithTermBody op fac term) = if (length fac > 1) 
                                                  then error "factor syntax not implemented yet"
                                                  else MArithTernary (translateArithTermOp op) (translateSimpleArith (head fac)) (translateArithTerm term)

translateSimpleArith :: JArithSimpl -> MArith
translateSimpleArith (JArithVar v) = MArithVar v
translateSimpleArith (JMinusExpr simp) = MArithMinus $ (translateSimpleArith simp)
translateSimpleArith (JSimplExpr expr) = MArithParens $ translateArithExpr expr
translateSimpleArith (JArithNumber n) = case n of
    Left i -> MArithInt i
    Right f -> MArithDouble f 

translateArithExprOp op = case op of
    AddOp -> "+"
    MinusOp -> "-"

translateArithTermOp op = case op of
    MultOp -> "*"
    SlashOp -> "/"
    DivOp -> "div"
    ModOp -> "mod"
    
translateArithExpr :: JArithExpr -> MArith
translateArithExpr (JArithExprEnd aterm) = translateArithTerm aterm
translateArithExpr (JArithExprExpr op aterm aexpr) = MArithTernary (translateArithExprOp op) (translateArithTerm aterm) (translateArithExpr aexpr)

translateTerm :: JTerm -> MTerm
translateTerm (JTermLit (JLit neg af)) = translateAtomForm af
translateTerm (JTermList jlist) = translateList jlist
translateTerm (JTermArithExpr aex) = MTArith $ translateArithExpr aex
translateTerm (JTermVar v) = MTVar v
translateTerm (JTermString s) = MTString s

translateList :: JList -> MTerm
translateList (JLCell terms) = MList (map translateTerm terms)
translateList (JLOpts terms listOrVar) = MList (map translateTerm terms) -- ignore list opts for the time being

translateRelOp op = case op of
    JLtOp -> "<"
    JLtEOp -> "<="
    JGtOp -> ">"
    JGtEOp -> ">="
    JDblEqOp -> "="
    JSlashDblEqOp -> error "I'm still unsure what this operator does"
    JEqOp -> error "explicit unification is currently unsupported"

translateRelTerm :: Either JLiteral JArithExpr -> Either MTerm MArith
translateRelTerm (Left (JLit neg af)) =  Left $ translateAtomForm af
translateRelTerm (Right jaex) = Right $ translateArithExpr jaex

translateRelExpr (JRelExprBody op relterm relterm') = MLogRelex (translateRelOp op) (translateRelTerm relterm) (translateRelTerm relterm')

translateSimpleLogExpr :: JSimpleLogExpr -> MLogEx
translateSimpleLogExpr (JSimpleLogLit (JLit neg af)) = MLogSimple $ translateAtomForm af
translateSimpleLogExpr (JSimpleLogRelExpr relex) = translateRelExpr relex
translateSimpleLogExpr (JSimpleLogVar v) = MLogSimple $ MTVar v

translateLogEx :: JLogExpr -> MLogEx
translateLogEx (JSimpleLog l) = translateSimpleLogExpr l
translateLogEx (JLogNot ex) = MLogNot $ translateLogEx ex
translateLogEx (JLogOr ex ex') = MLogOr (translateLogEx ex) (translateLogEx ex')
translateLogEx (JLogAnd ex ex') = MLogAnd (translateLogEx ex) (translateLogEx ex')
translateLogEx (JLogParens ex) = MLogParens $ translateLogEx ex


-- ignoring message source syntax for now
    
translateRule :: JRule -> MClause
translateRule ((JLit neg af),logex) = MClause (translateAtomForm af) (translateLogEx logex)
     
    
-- Translations from AAMAS 2014 paper


r1 = "\n\nplan(I,_), not intention(I) ->> delete-plan(I).\n" ++
    "intention(I), remainder(I,[]), justification(I,J), not subgoal(_,J), atom(Id,J) ->> delete-atom(Id).\n" ++
    "intention(I), remainder(I,[]), justification(I,J), subgoal(K,J), substitution(I,Si), substitution(K,Sk),\n" ++
    "  merge(Si,Sk,Sik) ->> set-substitution(K, Sik), delete-atom(J).\n" ++
    "cycle(C), not selected-event(_,C), trigger-event(I) ->> add-atom(selected-event(I,C)).\n\n"

r3 = "\n\ncycle(C), selected-event(I,C), not (justification(Ji, I), intention(Ji)), justification(J,I)\n" ++
    "    ->> add-intention(J).\n" ++ 
    "not scheduled(_), executable-intention(I) ->> schedule(I).\n" ++
    "scheduled(I), remainder(I, [Q | Pi]), not Q ->> set-remainder(I, [!(+test(Q)) | Pi]).\n\n"


d = "\n\nadd-belief(B) = add-atom(belief(B)), add-atom(+belief(B)).\n" ++
    "delete-belief(B) = atom(I,belief(B)), delete-atom(I), add-atom(-belief(B)).\n" ++
    "scheduled(I) <- state(I,F), member(scheduled, F).\n" ++
    "intention(I) <- state(I,F), member(intended, F).\n" ++
    "executable-intention(I) <- intention(I), not subgoal(I,_).\n\n"


translateTrigOpA op = case op of
    JTriggerPlus -> "+"
    JTriggerMinus -> "-"
    JTriggerMinusPlus -> "-+"

translateTrigOpB op = case op of
    JTriggerGoal -> "!"
    JTriggerQuery -> "?"

-- ignoring annots again for now
--translateTriggerEvent (JTriggerEvent opA maybeOpB (JLit neg (JAtmFrm pred args annots))) =
--    MAtom
--    (appendPred pred $ (translateTrigOpA opA ++ unMaybe maybeOpB))
--    (map translateTerm args)
--        where
--            unMaybe (Just op) = translateTrigOpB op
--            unMaybe Nothing = ""
--            appendPred (Left s) str = str ++ s
--            appendPred (Right s) str = str ++ s
--

magic opA opB a@(MAtom pred args) = case (opA,opB) of
    (JTriggerPlus, Just JTriggerGoal) -> MAtom "+goal" [a]
    --(JTriggerMinus, JTriggerGoal) -> MAtom "-goal" [a]
    (JTriggerPlus, Just JTriggerQuery) -> MAtom ("+?" ++ pred) args
    --(JTriggerMinus, JTriggerQues) -> MAtom ("-?" ++ pred) args
    (JTriggerPlus, Nothing) -> MAtom ("+" ++ pred) args
    (JTriggerMinus, Nothing) -> MAtom ("-" ++ pred) args
    --(JTriggerMinusPlus, _) -> ...
    -- can translate this to a trigger for - and a separate trigger for +, but the type of this function will change from MAtom to [MAtom] 

translateTriggerEvent (JTriggerEvent opA maybeOpB (JLit neg a)) =
    magic opA maybeOpB (translateAtomForm a)

-- write: translation for subgoal actions (!g -> !+goal(g)), test-goal -> query, +b -> add-belief, -b -> delete-belief

translateBodyLit JExOp a = [ MAtom "!+goal" [a] ]
translateBodyLit JQuesOp a = [ MAtom "?+belief" [a] ]
translateBodyLit JPlusOp a = [ MAtom "add-belief" [a] ]
translateBodyLit JMinusOp a = [ MAtom "delete-belief" [a] ]
translateBodyLit JMinusPlusOp a = [ MAtom "delete-belief" [a], MAtom "add-belief" [a]]

translateBodyForm (JBodyAtm a) = [ (translateAtomForm a) ]
translateBodyForm (JBodyVar v) = [ (MTVar v) ]
translateBodyForm (JBodyRelExpr relex) = [ (MAtom "?" [MAtom (show $ translateRelExpr relex) []]) ]
translateBodyForm (JBodyLit op (JLit neg af)) = translateBodyLit op $ translateAtomForm af
        
translateBody (JBody (Left bodyforms)) = concat $ map translateBodyForm bodyforms
translateBody (JBody (Right JTrue)) = []


translatePlan :: JPlan -> MORule
translatePlan (JPlan maybeAf trig maybeContext maybeBody) =
    MORule [translateTriggerEvent trig] (translateContext maybeContext) (translateBodyMaybe maybeBody)
        where
            translateContext (Just (JContext c)) = Just $ translateLogEx c
            translateContext Nothing = Nothing
            translateBodyMaybe (Just b) = translateBody b
            translateBodyMaybe Nothing = []

-- does the translation for plan contexts in AAMAS 2014 make sense?
-- what if the plan context element isn't a belief, but a rule execution (ie a query)
-- could offer a clause as part of translation (in D) that tries 'belief' as a query against atom instances before trying the atom as a clause/rule query

-- ignoring negation and annots again
translateInitBelief :: JInitBelief -> [MInitBelief]
translateInitBelief (JLit neg af) = [MAtom "belief" [translateAtomForm af],
                                 MAtom "+belief" [translateAtomForm af]]

translateInitGoal :: JInitGoal -> MInitBelief
translateInitGoal (JLit neg af) = MAtom "!+goal" [translateAtomForm af]

translateAgent :: JAgent -> MAgent
translateAgent JAgent {jgoals = goals, jbeliefs = beliefs, jrules = rules, jplans = plans} =
    MAgent {mbeliefs = (concat (map translateInitBelief beliefs)) ++ (map translateInitGoal goals),
            morules = map translatePlan plans,
            mclauses = map translateRule rules}

sagent MAgent {mbeliefs = beliefs, morules = orules, mclauses = clauses} =
    (intercalate "\n" (map show clauses)) ++
    d ++
    (intercalate "\n" (map (\s -> s ++ ".") (map show beliefs))) ++
    r1 ++
    (intercalate "\n" (map show orules)) ++
    r3
    
instance Show MAgent where show x = sagent x
    
translateJasonSourceToMetaAPL :: Source -> Source
translateJasonSourceToMetaAPL jas = show $ translateAgent $ parseAgent "" jas

translateJasonFileToMetaAPLSource :: Path -> IO Source
translateJasonFileToMetaAPLSource path = do
    jag <- parseAgentFromFile path
    return $ show $ translateAgent jag

translateJasonFileToMetaAPLFile :: Path -> Path -> IO ()
translateJasonFileToMetaAPLFile source target = do
    maplSource <- translateJasonFileToMetaAPLSource source
    writeFile target maplSource 
