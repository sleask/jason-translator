module MetaAPL.MetaAPLAST where

import Data.List (intercalate)

type MVar = String
type MString = String
type MID = Integer
type MPred = String
type MConst = Either String Integer
type ArithOp = String
type RelOp = String

type MInitBelief = MTerm
type MInitGoal = MTerm

data MArith = MArithTernary ArithOp MArith MArith
            | MArithInt Integer
            | MArithDouble Double
            | MArithParens MArith
            | MArithVar MVar
            | MArithMinus MArith

type MRelTerm = (Either MTerm MArith)

data MLogEx = MLogAnd MLogEx MLogEx
            | MLogOr MLogEx MLogEx
            | MLogParens MLogEx
            | MLogNot MLogEx
            | MLogSimple MTerm
            | MLogRelex RelOp MRelTerm MRelTerm

data MTerm = MAtom MPred [MTerm]
            | MList [MTerm]
            | MTArith MArith
            | MTVar MVar
            | MTString MString
            

data MORule = MORule [MTerm] (Maybe MLogEx) [MTerm]

data MClause = MClause MTerm MLogEx

data MAgent = MAgent {mbeliefs :: [MTerm],
                     morules :: [MORule],
                     mclauses :: [MClause]}


sterm x = case x of 
    MAtom pred ts       -> pred ++ (if length ts > 0 then 
                                           "(" ++ (intercalate ", " (map show ts)) ++ ")"
                                            else "")
    MList ts            -> "[" ++ (intercalate ", " (map show ts)) ++ "]"
    MTArith ar          -> show ar
    MTString s          -> s
    MTVar v             -> v

sarith x = case x of
    MArithTernary op l r -> (show l) ++ " " ++ op ++ " " ++ (show r)
    MArithInt n -> show n
    MArithDouble n -> show n
    MArithParens a -> "(" ++ show a ++ ")"
    MArithVar v -> show v
    MArithMinus x -> "-" ++ show x


slogex x = case x of
    MLogAnd l r -> show l ++ ", " ++ show r
    MLogOr l r -> show l ++ " | " ++ show r
    MLogParens ex -> "(" ++ show ex ++ ")"
    MLogNot ex -> "not " ++ show ex
    MLogSimple t -> show t
    MLogRelex op l r -> show l ++ " " ++ op ++ " " ++ show r

srelterm (Left mterm) = show mterm
srelterm (Right arith) = show arith

sorule x = case x of
    MORule reasons context body -> intercalate ", " (map show reasons)
                                    ++ context' context
                                    ++ " -> "
                                    ++ intercalate "; " (map show body)
                                    ++ "."
    where
        context' con = case con of
            Just c -> " : " ++ show c
            Nothing -> ""

sclause x = case x of
    MClause head body -> show head ++ " <- " ++ show body ++ "."


instance Show MTerm where show x = sterm x
instance Show MArith where show x = sarith x
instance Show MLogEx where show x = slogex x
instance Show MORule where show x = sorule x
instance Show MClause where show x = sclause x

