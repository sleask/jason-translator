module Jason.JasonAST where 

type JVar = String
type JNumber = Either Integer Double
type JString = String
data JTrue = JTrue
        deriving (Show)
data JFalse = JFalse
        deriving (Show)
data JAtom = JAtom String deriving (Show)

data JArithSimpl = JArithNumber JNumber
                | JArithVar JVar 
                | JMinusExpr JArithSimpl
                | JSimplExpr JArithExpr
        deriving (Show)

type JArithFactor = [JArithSimpl]
data JArithTermOp = MultOp | SlashOp | DivOp | ModOp 
        deriving (Show)
                

data JArithTerm = JArithTermEnd JArithFactor
                | JArithTermBody JArithTermOp JArithFactor JArithTerm 
        deriving (Show)
               

data JArithExprOp = AddOp | MinusOp 
        deriving (Show)
data JArithExpr = JArithExprEnd JArithTerm 
                | JArithExprExpr JArithExprOp JArithTerm JArithExpr 
        deriving (Show)

data JTerm = JTermLit JLiteral 
            | JTermList JList
            | JTermArithExpr JArithExpr
            | JTermVar JVar
            | JTermString JString
        deriving (Show)

data JLiteral = JLit Bool JAtomicForm -- bool is true when '~' (not) present on the form
        deriving (Show)

data JRelExprOp = JLtOp | JLtEOp | JGtOp | JGtEOp | JDblEqOp | JSlashDblEqOp | JEqOp
        deriving (Show)
data JRelExpr = JRelExprBody JRelExprOp JRelTerm JRelTerm
        deriving (Show)
type JRelTerm = Either JLiteral JArithExpr

data JList = JLCell [JTerm] | JLOpts [JTerm] (Either JList JVar) 
        deriving (Show)

data JAtomicForm = JAtmFrm (Either String JVar) [JTerm] (Maybe [JTerm]) 
        deriving (Show)

data JBodyOp = JExOp | JQuesOp | JPlusOp | JMinusOp | JMinusPlusOp 
        deriving (Show)
data JBodyForm = JBodyLit JBodyOp JLiteral | JBodyAtm JAtomicForm | JBodyVar JVar | JBodyRelExpr JRelExpr 
        deriving (Show)
data JBody = JBody (Either [JBodyForm] JTrue)
        deriving (Show)

data JSimpleLogExpr = JSimpleLogLit JLiteral | JSimpleLogRelExpr JRelExpr | JSimpleLogVar JVar
        deriving (Show)
data JLogExpr = JSimpleLog JSimpleLogExpr | JLogNot JLogExpr | JLogAnd JLogExpr JLogExpr | JLogOr JLogExpr JLogExpr | JLogParens JLogExpr
        deriving (Show)

data JContext = JContext JLogExpr
        deriving (Show)

data JTriggerOpA = JTriggerPlus | JTriggerMinus | JTriggerMinusPlus
        deriving (Show)
data JTriggerOpB = JTriggerGoal | JTriggerQuery
        deriving (Show)

data JTriggerEvent = JTriggerEvent JTriggerOpA (Maybe JTriggerOpB) JLiteral
        deriving (Show)

data JPlan = JPlan (Maybe JAtomicForm) JTriggerEvent (Maybe JContext) (Maybe JBody)
        deriving (Show)

type JInitGoal = JLiteral
type JInitBelief = JLiteral
type JInitBels = ([JInitBelief],[JRule])
type JRule = (JLiteral,JLogExpr)

data ParseAgent = JPGoal JInitGoal | JPBelief JInitBelief | JPRule JRule | JPPlan JPlan
    deriving (Show)

data JAgent = JAgent {jgoals :: [JInitGoal],
                    jbeliefs :: [JInitBelief],
                    jrules :: [JRule],
                    jplans :: [JPlan]}
    deriving (Show)


--
-- need to add support for annotations
-- e.g. +!goal(...)[source(...)]
-- just needs an appropriate translation
--
-- problem: elements of program are not stored in parse order
