module Jason.JasonParser where

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Control.Applicative ((<$>), (<*>))
import Data.Either (lefts, rights)
--import Text.ParserCombinators.Parsec.Expr
--import Text.ParserCombinators.Parsec.Language
--import Data.Maybe
import Data.List (intercalate)
import Jason.JasonAST

import qualified Text.Parsec.Token as P
import Text.Parsec.String
import Text.Parsec.Language


-- Lexer

identS = letter <|> char '.'
identL = alphaNum <|> letter <|> char '_'
resIdents = ["not"] --"true","false"]
resOps = ["div","mod",">","<","<=",">=","==","\\==","=","+","-","*","/",":-",".","**","&","|","<-",":","@"]

langDef :: P.LanguageDef st
langDef = emptyDef { P.commentStart = "/*",
                        P.commentEnd = "*/",
                          P.commentLine = "//",
                          P.nestedComments = False,
                          P.identStart = identS,
                          P.identLetter = identL,
                          P.opStart = oneOf "",
                          P.opLetter = oneOf "",
                          P.reservedNames = resIdents,
                          P.reservedOpNames = resOps,
                          P.caseSensitive = True }

lexer = P.makeTokenParser langDef

naturalOrFloat :: Parser (Either Integer Double)
naturalOrFloat = P.naturalOrFloat lexer

identifier :: Parser String
identifier = P.identifier lexer

stringLiteral :: Parser String
stringLiteral = P.stringLiteral lexer

parens :: Parser a -> Parser a
parens = P.parens lexer

reserved :: String -> Parser ()
reserved = P.reserved lexer

reservedOp :: String -> Parser ()
reservedOp = P.reservedOp lexer

sqBrackets :: Parser a -> Parser a
sqBrackets = P.brackets lexer

comma :: Parser String
comma = P.comma lexer

semi :: Parser String
semi = P.semi lexer

colon :: Parser String
colon = P.colon lexer

semiSep :: Parser a -> Parser [a]
semiSep = P.semiSep lexer

commaSep :: Parser a -> Parser [a]
commaSep = P.commaSep lexer

whitespace = P.whiteSpace lexer

var' :: Parser JVar
var' = do
        x <- upper
        xs <- option [] identifier
        return (x:xs)

var :: Parser JVar
var = try var' <|> try (string "_" >> var') <|> string "_"

namespacedAtom = do
    idents <- sepBy1 identifier (char '.')
    return $ intercalate "." idents
-- surprised this actually works


atom = try (JAtom <$> namespacedAtom) <|> (JAtom <$> identifier)

-- in the case that we have an internal action eg. .desire(...)
-- convert to 'dot-act' eg. dot-act(desire(...)) and handle in interpreter

transformActionSyntax (JAtmFrm pred args annot) =
  case pred of
    Left p -> 
      if (head p) == '.'
        then JAtmFrm (Left "dot-act") [(JTermLit $ JLit False $ JAtmFrm (Left (tail p)) args annot)] Nothing
        else JAtmFrm pred args annot
    Right _ -> JAtmFrm pred args annot


atomicForm = do
    pred <- (Right <$> var) <|> (Left <$> identifier) 
    args <- optionMaybe $ char '(' >> commaSep term >>= \ts -> char ')' >> return ts
    ann <- optionMaybe $ sqBrackets listBody
    return $ case args of 
        Just args' -> transformActionSyntax $ JAtmFrm pred args' ann
        Nothing -> transformActionSyntax $ JAtmFrm pred [] ann

literal = do
    not <- optionMaybe $ char '~'
    a <- atomicForm
    return $ case not of
        Just _ -> JLit True a
        Nothing -> JLit False a

doubleAst = reservedOp "**"


arithmSimple = (JSimplExpr <$> parens arithmExpr)
        <|> (JMinusExpr <$> (char '-' >> arithmSimple))
        <|> (JArithVar <$> var)
        <|> (JArithNumber <$> naturalOrFloat)
                        
arithmFactor = sepBy arithmSimple (whitespace >> doubleAst >> whitespace)

arithmOp = (reservedOp "*" >> return MultOp)
        <|> (reservedOp "/" >> return SlashOp)
        <|> (reservedOp "div" >> return DivOp)
        <|> (reservedOp "mod" >> return ModOp)

arithmExprOp =  (reservedOp "+" >> return AddOp)
            <|> (reservedOp "-" >> return MinusOp)

arithmTermBody = do
        f <- arithmFactor
        op <- arithmOp
        t <- arithmTerm
        return $ JArithTermBody op f t

arithmTerm = try arithmTermBody <|> (JArithTermEnd <$> arithmFactor)

arithmExprExpr  = do
        t <- arithmTerm
        op <- arithmExprOp
        ex <- arithmExpr
        return $ JArithExprExpr op t ex

arithmExpr = try arithmExprExpr <|> (JArithExprEnd <$> arithmTerm)

-- may be amenable to chainl1 instead to remove left recursion from grammar

listBody = commaSep term
listPattern = whitespace >> char '|' >> whitespace >> (try (Left <$> list) <|> (Right <$> var)) 

list = sqBrackets (try (JLOpts <$> listBody <*> listPattern) <|> (JLCell <$> listBody))

term = (JTermList <$> list)
    <|> (JTermVar <$> var)
    <|> (JTermLit <$> literal)
    <|> (JTermString <$> stringLiteral)
    <|> (JTermArithExpr <$> arithmExpr)


relExprOp = try (reservedOp "<=" >> return JLtEOp)
         <|> try (reservedOp ">=" >> return JGtEOp)
         <|> (reservedOp ">" >> return JGtOp)
         <|> (reservedOp "<" >> return JLtOp)
         <|> try (reservedOp "==" >> return JDblEqOp)
         <|> (reservedOp "=" >> return JEqOp)
         <|> (reservedOp "\\==" >> return JSlashDblEqOp)

relTerm = (Left <$> literal) <|> (Right <$> arithmExpr)

relExpr = do
    t <- relTerm
    whitespace
    op <- relExprOp
    whitespace
    t' <- relTerm
    return $ JRelExprBody op t t'

bodyFormOp = (string "!" >> return JExOp)
           <|> (string "?" >> return JQuesOp)
           <|> try (string "-+" >> return JMinusPlusOp)
           <|> (string "+" >> return JPlusOp)
           <|> (reservedOp "-" >> return JMinusOp)

bodyFormula = (JBodyLit <$> bodyFormOp <*> literal)
           <|> (JBodyVar <$> var)
           <|> (JBodyAtm <$> atomicForm)
           <|> (JBodyRelExpr <$> relExpr)

body = try (reserved "true" >> return (JBody $ Right JTrue)) <|> (JBody <$> Left <$> semiSep bodyFormula) 

logExpr = andExpr

andExpr = do
    ex <- orExpr
    ex' <- optionMaybe $ try andExpr'
    return $ case ex' of
        Just e -> JLogAnd ex e
        Nothing -> ex

andExpr' = do
    P.lexeme lexer $ reservedOp "&"
    ex <- orExpr 
    ex' <- optionMaybe andExpr'
    return $ case ex' of
        Just e -> JLogAnd ex e
        Nothing -> ex

orExpr = do
    ex <- notExpr 
    ex' <- optionMaybe $ try orExpr'
    return $ case ex' of
        Just e -> JLogOr ex e
        Nothing -> ex

orExpr' = do
    P.lexeme lexer $ reservedOp "|"
    ex <- notExpr 
    ex' <- optionMaybe orExpr'
    return $ case ex' of
        Just e -> JLogOr ex e
        Nothing -> ex

notExpr = try (P.lexeme lexer (reserved "not") >> andExpr >>= \a -> return $ JLogNot a) 
       <|> P.lexeme lexer simple
       <|> (JLogParens <$> parens andExpr) 
    
simple = JSimpleLog <$>
        (try (JSimpleLogRelExpr <$> relExpr)
         <|> try (JSimpleLogVar <$> var)
         <|> (JSimpleLogLit <$> literal))

context = JContext <$> logExpr

triggerOpA = (char '+' >> return JTriggerPlus)
          <|> try (char '-' >> char '+' >> return JTriggerMinusPlus)
      <|> (char '-' >> return JTriggerMinus)

triggerOpB = (char '?' >> return JTriggerQuery)
          <|> (char '!' >> return JTriggerGoal)

triggeringEvent = do
    opA <- triggerOpA
    opB <- optionMaybe triggerOpB
    l <- literal
    return $ JTriggerEvent opA opB l

plan = do
    name <- optionMaybe $ reservedOp "@" >> atomicForm
    tr <- P.lexeme lexer triggeringEvent
    c <- optionMaybe $ reservedOp ":" >> context
    b <- optionMaybe $ reservedOp "<-" >> body 
    P.lexeme lexer $ char '.'
    return $ JPlan name tr c b


goal ::  Parser JLiteral
goal = do
    char '!'
    l <- literal
    whitespace
    char '.'
    return l

init_goal = whitespace >> goal

belief :: Parser JInitBelief
belief = do
    l <- literal
    char '.'
    return l

init_belief = whitespace >> belief

rule = do
    l <- P.lexeme lexer literal
    reservedOp ":-"
    ex <- P.lexeme lexer logExpr
    char '.'
    return (l, ex)

roughAgent = many
        (try (JPPlan <$> (whitespace >> plan))
        <|> try (JPRule <$> (whitespace >> rule))
        <|> try (JPGoal <$> (whitespace >> goal))
        <|> (JPBelief <$> (whitespace >> belief)))

roughStep ag x = case x of
    JPGoal g -> ag { jgoals = g : jgoals ag }
    JPBelief b -> ag { jbeliefs = b : jbeliefs ag }
    JPRule r -> ag { jrules = r : jrules ag }
    JPPlan p -> ag { jplans = p : jplans ag }

parseAgent path body = if (length e > 0)
    then error (show $ head e)
    else foldl roughStep ag (head successes)
    where
        parses = [(parse roughAgent path body)]
        e = lefts parses
        successes = rights parses
        ag = JAgent {jgoals = [], jbeliefs = [], jrules = [], jplans = []}

parseAgentFromFile path = do
        f <- readFile path 
        return $ parseAgent path f
